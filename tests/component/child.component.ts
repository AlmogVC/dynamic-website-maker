import { Component } from "../../src/models/component.model.js";
import { EventEmitter } from "../../src/utils/event-emitter.js";

export class ChildComponent extends Component {
  outputs = {
    addSomething: new EventEmitter(this.element.id, 'addSomething'),
  };

  constructor() {
    super();
  }

  addElements() {
    const buttonElement: HTMLButtonElement = document.createElement("button");
    buttonElement.textContent = "Dispatch Add Something Event";
    buttonElement.addEventListener("click", this.onButtonClick.bind(this));

    this.addChild(buttonElement);
  }

  style() {
    this.element.style.border = "2px solid black";
  }

  onButtonClick() {
      this.outputs.addSomething.emit({some: 'thing'});
  }
}
