import { Component } from '../../src/models/component.model.js';
import { TaskComponent } from './task/task.component.js';

export class TaskListComponent extends Component {
  addElements() {
    const nameInputElement: HTMLInputElement = document.createElement('input');
    const descriptionInputElement: HTMLInputElement = document.createElement('input');
    nameInputElement.placeholder = 'Name';
    descriptionInputElement.placeholder = 'Description';
    this.addChild(nameInputElement);
    this.addChild(descriptionInputElement);

    const buttonElement: HTMLButtonElement = document.createElement('button');
    buttonElement.textContent = 'Add Task';

    const changeTextButtonElement: HTMLButtonElement = document.createElement('button');
    changeTextButtonElement.textContent = 'Change Tasks';
    const tasks: TaskComponent[] = [];

    buttonElement.addEventListener('click', () => {
      const taskComponent: TaskComponent = new TaskComponent();
      this.addChild(taskComponent);
      taskComponent.inputs.name = nameInputElement.value;
      taskComponent.inputs.description = descriptionInputElement.value;
      tasks.push(taskComponent);
    });

    this.addChild(buttonElement);

    changeTextButtonElement.addEventListener('click', () => {
      tasks.forEach(task => {
        task.inputs.name = nameInputElement.value;
        task.inputs.description = descriptionInputElement.value;
      });
    });

    this.addChild(changeTextButtonElement);

  }
}
