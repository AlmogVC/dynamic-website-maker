import { Component } from '../../src/models/component.model.js';
import { IdProvider } from '../../src/utils/id.provider.js';
import { ChildComponent } from './child.component.js';

export class ParentComponent extends Component {
    constructor() {
        super();
    }

    addElements() {
        const childComponent: ChildComponent = new ChildComponent();

        childComponent.outputs.addSomething.register(this.element, () => {
            const newChildComponent: ChildComponent = new ChildComponent();
            this.addChild(newChildComponent);
        });

        this.addChild(childComponent);
    }

    style() {
        this.element.style.display = "flex";
        this.element.style.flexWrap = "wrap";
    }
}

