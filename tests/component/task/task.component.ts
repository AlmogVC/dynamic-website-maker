import { Component } from "../../../src/models/component.model.js";

export class TaskComponent extends Component {
  static htmlFilePath = 'tests/component/task/task.component.html';
  inputs = {
    name: "",
    description: ""
  };

  addElements() {
    // const nameElement: HTMLParagraphElement = document.createElement("p");
    // const descriptionElement: HTMLParagraphElement = document.createElement('p');

    // nameElement.textContent = this.inputs.name;
    // descriptionElement.textContent = this.inputs.description;

    // this.addChild(nameElement);
    // this.addChild(descriptionElement);
    // this.inputsEvents.name.register(this.element, data => {
    //   nameElement.textContent = data.detail;
    // });

    // this.inputsEvents.description.register(this.element, data => {
    //   descriptionElement.textContent = data.detail;
    // });
  }

  style() {
    this.element.style.border = "2px solid black";
  }

  onInputChange() {}
}
