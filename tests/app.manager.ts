import { App } from "../src/models/app.model.js";
import { Component } from "../src/models/component.model.js";
import { TaskListComponent } from "./component/task-list.component.js";
import { AppModule } from "../src/models/module.mode.js";
import { TaskComponent } from "./component/task/task.component.js";

export class AppManager {
  mainComponent: Component;
  mainElementId: string;

  constructor(mainComponent: Component, mainElementId: string) {
    this.mainComponent = mainComponent;
    this.mainElementId = mainElementId;
  }

  static create() {
    const mainComponent = new Component();
    const appManager: AppManager = new AppManager(mainComponent, 'app-container');

    const app = new App(appManager);
    
    app.init();
    appManager.init();
  }

  getNode() {
    return this.mainComponent.getNode();
  }

  async init() {
    const tasksModule: AppModule = new AppModule([TaskComponent, TaskListComponent], TaskListComponent);
    await tasksModule.load();

    const mainModuleComponent = tasksModule.init();
    this.mainComponent.init();
    this.mainComponent.addChild(mainModuleComponent);
  }
}
