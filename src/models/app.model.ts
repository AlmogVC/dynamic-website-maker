import { AppManager } from "../../tests/app.manager.js";

export class App {
  appManager: AppManager;

  constructor(appManager: AppManager) {
    this.appManager = appManager;
  }

  init() {
    const appContainer: HTMLElement | null = document.getElementById(
      this.appManager.mainElementId
    );

    if (!appContainer) throw new Error(`${this.appManager.mainElementId} element was not found`);
    appContainer.append(this.appManager.getNode());
  }
}
