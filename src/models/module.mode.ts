import { Component } from "./component.model.js";

export class AppModule {
  mainComponentInstance?: Component;

  constructor(
    private components: typeof Component[],
    public mainComponentClass: typeof Component
  ) {
  }

  load() {
    const promises: Promise<void>[] = [];

    this.components.forEach(component => {
      promises.push(component.loadHtml(component.htmlFilePath || ''));
    });

    return Promise.all(promises);
  }

  init() {
    this.mainComponentInstance = new this.mainComponentClass();
    return this.mainComponentInstance;
  }
}
