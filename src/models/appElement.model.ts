import { IdProvider } from "../utils/id.provider";

export class AppElement<K extends keyof HTMLElementTagNameMap> {
    protected id: string;
    protected element: HTMLElement;

    constructor(tagName: K, options?: ElementCreationOptions) {
        this.element = document.createElement(tagName, options);
        this.id = IdProvider.getId();
        this.element.id = this.id;
    }

    init() {
        this.element = document.getElementById(this.element.id) as HTMLElement;

        if (!this.element) throw new Error(`Element with id '${this.id}' does not exist`);

    }
}