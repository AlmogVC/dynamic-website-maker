import { IdProvider } from "../utils/id.provider.js";
import { EventEmitter } from "../utils/event-emitter.js";

export class Component {
  protected id: string;
  protected element: HTMLDivElement;
  protected children: (HTMLElement | Component)[] = [];
  outputs: { [key: string]: EventEmitter } = {};
  inputs: { [key: string]: any } = {};
  inputsEvents: { [key: string]: EventEmitter } = {};
  static template: string;
  static htmlFilePath?: string;

  constructor() {
    this.element = document.createElement("div");
    this.id = IdProvider.getId();
    this.element.id = this.id;
  }

  setHtmlFromTemplate() {
    if (!Component.template) return;
  
    this.element.innerHTML = Component.template;
  }

  static loadHtml(htmlFilePath: string): Promise<void> {
    const loadPromise: Promise<void> = new Promise<void>((resolve, reject) => {
      if (!htmlFilePath) {
        resolve();
        return;
      }

      const xhr = new XMLHttpRequest();
      xhr.open("GET", htmlFilePath, true);
      xhr.onreadystatechange = function() {
        if (this.readyState !== 4) return;
        if (this.status !== 200) {
          reject(
            `Component html loading error, returned status code: ${this.status}`
          );
        }

        Component.template = this.responseText;
        resolve();
      };
      xhr.send();
    });

    return loadPromise;
  }

  private setElementValue(element: Element, value: string) {
    if (element instanceof HTMLInputElement) {
      element.value = value;
      return;
    }

    element.textContent = value;
  }

  parseTemplate() {
    if (!Component.template) return;
    if (!this.element) return;

    // parse inputs
    // parse local variables
    // parse contained components
    // parse outputs
  }

  initTemplateInputs() {
    if (!Component.template) return;
    if (!this.element) return;

    Object.keys(this.inputs).forEach(input => {
      const elements = this.element.querySelectorAll(`[app-value="${input}"]`);
      if (!elements) return;

      elements.forEach(element => {
        this.inputsEvents[input].register(this.element, data => {
          this.setElementValue(element, data.detail);
        });
      });
    });
  }

  initInputsProxy() {
    this.inputs = new Proxy(this.inputs, {
      set: (target, key, value) => {
        const keyAsString: string = String(key);
        this.onInputChange(keyAsString, value);
        this.inputsEvents[keyAsString].emit(value);
        target[keyAsString] = value;
        return true;
      }
    });
  }

  init() {
    this.element = document.getElementById(this.element.id) as HTMLDivElement;

    if (!this.element) {
      throw new Error(`Element with id '${this.id}' does not exist`);
    }

    this.initInputsProxy();
    this.onInit();
    this.addInputsEventEmitters();

    if (Component.template) {
      this.setHtmlFromTemplate();
      this.initTemplateInputs();
    }

    this.addElements();
    this.style();
    this.initOutputs();
    this.initInputsEventEmitters();
  }

  onInputChange(key: string, value: any) {}

  private onInit() {}

  protected addElements() {}

  protected style() {}

  private initOutputs() {
    for (const key in this.outputs) {
      if (this.outputs.hasOwnProperty(key)) {
        this.outputs[key].init();
      }
    }
  }

  addInputsEventEmitters() {
    for (const key in this.inputs) {
      if (this.inputs.hasOwnProperty(key)) {
        this.inputsEvents[key] = new EventEmitter(
          this.element.id,
          `${this.element.id}-inputs-${key}-change`
        );
      }
    }
  }

  initInputsEventEmitters() {
    for (const key in this.inputsEvents) {
      if (this.inputsEvents.hasOwnProperty(key)) {
        this.inputsEvents[key].init();
      }
    }
  }

  addChild(child: Component | HTMLElement) {
    if (child instanceof Component) {
      this.element.append(child.getNode());
      child.init();
    } else if (child instanceof HTMLElement) this.element.append(child);

    this.children.push(child);
  }

  setStyle(style: string) {
    this.element.style.cssText = style;
  }

  getNode() {
    return this.element.cloneNode(true);
  }
}
