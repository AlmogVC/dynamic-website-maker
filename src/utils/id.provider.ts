export class IdProvider {
    static id: number = 1;

    static getId() {
        return `element-${IdProvider.id++}`;
    }
}