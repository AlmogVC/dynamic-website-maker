interface ElementHandler {
    element: HTMLElement,
    handler: (event: any) => void,
}

export class EventEmitter {
    elementId: string;
    name: string;
    elementsHandlers: ElementHandler[] = [];

    constructor(elementId: string, name: string) {
        this.elementId = elementId;
        this.name = name;
    }

    register(element: HTMLElement, handler: (data: any) => any) {
        this.elementsHandlers.push({element, handler});
    }

    init() {
        this.elementsHandlers.forEach(elementHandler => {
            const element = document.getElementById(elementHandler.element.id);

            if (element) {
                element.addEventListener(this.name, elementHandler.handler);
            }
        });
    }

    emit(data: any) {
        const element = document.getElementById(this.elementId);
        if (element) {
            element.dispatchEvent(new CustomEvent(this.name, { bubbles: true, detail: data }));
        }
    }
}